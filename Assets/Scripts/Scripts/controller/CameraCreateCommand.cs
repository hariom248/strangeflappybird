﻿using strange.extensions.command.impl;
using strange.extensions.context.api;
using strange.extensions.pool.api;
using UnityEngine;

public class CameraCreateCommand : Command
{
	[Inject(ContextKeys.CONTEXT_VIEW)]
	public GameObject contextView { get; set; }

	[Inject(GameElementPool.CAMERA_POOL)]
	public IPool<GameObject> cameraPool { get; set; }

    public override void Execute()
    {
		GameObject cameraGo = cameraPool.GetInstance();
		cameraGo.transform.parent = contextView.transform;
	}
}
