﻿using strange.extensions.command.impl;

public class GameRestartCommand : Command
{

    [Inject]
    public ISceneService sceneService { get; set; }

    public override void Execute()
    {
        sceneService.ReloadScene();
    }
}