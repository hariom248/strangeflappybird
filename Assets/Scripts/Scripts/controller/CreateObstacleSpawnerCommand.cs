﻿using strange.extensions.command.impl;

public class CreateObstacleSpawnerCommand : Command
{
	[Inject]
	public ISpawner spawner { get; set; }

	public override void Execute()
	{
		spawner.Start();
	}
}