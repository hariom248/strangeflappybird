﻿public interface ISpawner
{
    void Start();
    void Stop();
}
