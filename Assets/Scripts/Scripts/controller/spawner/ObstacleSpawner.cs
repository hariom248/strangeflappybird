﻿using strange.extensions.context.api;
using strange.extensions.pool.api;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Util;

public class ObstacleSpawner : ISpawner
{
	private readonly int numberOfObjects = 5;
	private readonly float recycleOffset = 8f;

	private Vector3 nextPosition;
	private Queue<Transform> objectQueue;

	[Inject]
	public IGameEngineRunner gameEngineRunner { get; set; }

	[Inject(ContextKeys.CONTEXT_VIEW)]
	public GameObject contextView { get; set; }

	[Inject(GameElementPool.OBSTACLE_POOL)]
	public IPool<GameObject> obstaclePool { get; set; }

	[Inject]
	public BirdModel birdModel { get; set; }

	[PostConstruct]
    public void PostConstruct()
    {
        objectQueue = new Queue<Transform>(numberOfObjects);
        for (int i = 0; i < numberOfObjects; i++)
        {
            var t = obstaclePool.GetInstance().transform;
            t.position = new Vector3(0, 0, -100f);
            objectQueue.Enqueue(t);
        }

        nextPosition = birdModel.Position;
		nextPosition.x += recycleOffset;

        for (int i = 0; i < numberOfObjects; i++)
        {
            Recycle();
        }
    }

	public void Start()
	{
		gameEngineRunner.StartCoroutine(Spawn());
	}

	public void Stop()
	{
		gameEngineRunner.StopCoroutine(Spawn());
	}

	private IEnumerator Spawn()
	{
		while (true)
		{
			yield return null;
			if (objectQueue.Peek().localPosition.x + recycleOffset < birdModel.Position.x)
			{
				Recycle();
			}
		}
	}

	private void Recycle()
	{
		Transform o = objectQueue.Dequeue();
		o.position = nextPosition;
		nextPosition.x += 3f;
		nextPosition.y = Random.Range(4, 7);
		objectQueue.Enqueue(o);
	}
}