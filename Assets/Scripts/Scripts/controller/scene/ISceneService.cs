﻿using UnityEngine.SceneManagement;

public interface ISceneService
{
    void ReloadScene();
}
public class SceneService : ISceneService
{
    public void ReloadScene()
    {
        SceneManager.LoadScene(0);
    }
}