﻿using strange.extensions.command.impl;
using strange.extensions.context.api;
using strange.extensions.pool.api;
using UnityEngine;

public class BirdCreateCommand : Command
{
	[Inject(ContextKeys.CONTEXT_VIEW)]
	public GameObject contextView { get; set; }

	[Inject(GameElementPool.BIRD_POOL)]
	public IPool<GameObject> birdPool { get; set; }

    public override void Execute()
    {
		GameObject birdGo = birdPool.GetInstance();
		birdGo.transform.parent = contextView.transform;
	}
}
