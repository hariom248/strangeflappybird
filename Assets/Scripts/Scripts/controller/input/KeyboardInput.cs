﻿using UnityEngine;
using Util;

public class KeyboardInput : IInput
{
	[Inject]
	public IGameEngineRunner gameEngineRunner { get; set; }

	[Inject]
	public GameInputSignal gameInputSignal { get; set; }

	[PostConstruct]
	public void PostConstruct()
	{
		gameEngineRunner.AddUpdateListener(Update);
	}

	private void Update()
	{
		if (Input.GetKeyDown(KeyCode.Space))
		{
			gameInputSignal.Dispatch();
		}
	}

    ~KeyboardInput()
    {
		gameEngineRunner.RemoveUpdateListener(Update);
	}
}