﻿using strange.extensions.command.api;
using strange.extensions.command.impl;
using strange.extensions.context.impl;
using strange.extensions.pool.api;
using strange.extensions.pool.impl;
using UnityEngine;
using Util;

public class GameContext : MVCSContext
{
    public GameContext(MonoBehaviour contextView) : base(contextView)
    {
    }

    public override void Launch()
    {
        base.Launch();
        var startSignal = injectionBinder.GetInstance<GameStartSignal>();
        startSignal.Dispatch();
    }

    protected override void addCoreComponents()
    {
        base.addCoreComponents();
        injectionBinder.Unbind<ICommandBinder>();
        injectionBinder.Bind<ICommandBinder>().To<SignalCommandBinder>().ToSingleton();
    }

    protected override void mapBindings()
    {
        base.mapBindings();

        injectionBinder.Bind<IGameEngineRunner>().To<UnityRunner>().ToSingleton().CrossContext();
        injectionBinder.Bind<ISpawner>().To<ObstacleSpawner>().ToSingleton();
        injectionBinder.Bind<IInput>().To<KeyboardInput>().ToSingleton();
        injectionBinder.Bind<ISceneService>().To<SceneService>().ToSingleton();

        injectionBinder.Bind<IPool<GameObject>>().To<Pool<GameObject>>().ToSingleton().ToName(GameElementPool.OBSTACLE_POOL);
        injectionBinder.Bind<IPool<GameObject>>().To<Pool<GameObject>>().ToSingleton().ToName(GameElementPool.BIRD_POOL);
        injectionBinder.Bind<IPool<GameObject>>().To<Pool<GameObject>>().ToSingleton().ToName(GameElementPool.CAMERA_POOL);

        injectionBinder.Bind<GameInputSignal>().ToSingleton();

        injectionBinder.Bind<BirdModel>().ToSingleton();

        commandBinder.Bind<GameStartSignal>().
            To<GameStartCommand>().
            To<BirdCreateCommand>().
            To<CameraCreateCommand>().
            To<CreateObstacleSpawnerCommand>().
            InSequence().
            Once();

        commandBinder.Bind<GameRestartSignal>().To<GameRestartCommand>();

        mediationBinder.Bind<BirdView>().To<BirdViewMediator>();
        mediationBinder.Bind<CameraView>().To<CameraViewMediator>();
        mediationBinder.Bind<ObstacleView>().To<ObstacleViewMediator>();
    }

    protected override void postBindings()
    {
        base.postBindings();
        IPool<GameObject> obstaclePool = injectionBinder.GetInstance<IPool<GameObject>>(GameElementPool.OBSTACLE_POOL);
        obstaclePool.instanceProvider = new ResourceInstanceProvider("obstacle", 8);
        obstaclePool.inflationType = PoolInflationType.INCREMENT;

        IPool<GameObject> birdPool = injectionBinder.GetInstance<IPool<GameObject>>(GameElementPool.BIRD_POOL);
        birdPool.instanceProvider = new ResourceInstanceProvider("bird", 0);
        birdPool.inflationType = PoolInflationType.INCREMENT;

        IPool<GameObject> cameraPool = injectionBinder.GetInstance<IPool<GameObject>>(GameElementPool.CAMERA_POOL);
        cameraPool.instanceProvider = new ResourceInstanceProvider("camera", 0);
        cameraPool.inflationType = PoolInflationType.INCREMENT;
    }
}