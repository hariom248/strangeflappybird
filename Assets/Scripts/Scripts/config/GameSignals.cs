using strange.extensions.signal.impl;

public class GameInputSignal : Signal { }
public class GameStartSignal : Signal { }
public class GameRestartSignal : Signal { }
