﻿using System;
using System.Collections;
using UnityEngine;

namespace Util
{
    public class UnityRunner : IGameEngineRunner
    {
		private readonly UnityRunnerBehaviour unityBehaviour;
		private const string NAME = "Mono";
		public UnityRunner()
        {
			unityBehaviour = new GameObject(NAME).AddComponent<UnityRunnerBehaviour>();
		}

		public void AddFixedUpdateListener(Action act)
        {
			unityBehaviour.AddFixedUpdateListener(act);
        }

        public void AddLateUpdateListener(Action act)
        {
			unityBehaviour.AddLateUpdateListener(act);
		}

        public void AddUpdateListener(Action act)
        {
			unityBehaviour.AddUpdateListener(act);
		}

        public void RemoveFixedUpdateListener(Action act)
        {
			unityBehaviour.RemoveFixedUpdateListener(act);
		}

        public void RemoveLateUpdateListener(Action act)
        {
			unityBehaviour.RemoveLateUpdateListener(act);
		}

        public void RemoveUpdateListener(Action act)
        {
			unityBehaviour.RemoveUpdateListener(act);
		}

        public Coroutine StartCoroutine(IEnumerator method)
        {
			return unityBehaviour.StartCoroutine(method);
        }

        public void StopCoroutine(IEnumerator method)
        {
			unityBehaviour.StopCoroutine(method);
		}
    }

    public class UnityRunnerBehaviour : MonoBehaviour
	{
		private Action onUpdate;
		private Action onLateUpdate;
		private Action onFixedUpdate;

		private void Update()
		{
			onUpdate?.Invoke();
		}

		private void FixedUpdate()
		{
			onFixedUpdate?.Invoke();
		}

		private void LateUpdate()
		{
			onLateUpdate?.Invoke();
		}

		public void AddUpdateListener(Action act)
		{
			onUpdate += act;
		}

		public void RemoveUpdateListener(Action act)
		{
			onUpdate -= act;
		}

		public void AddFixedUpdateListener(Action act)
		{
			onFixedUpdate += act;
		}

		public void RemoveFixedUpdateListener(Action act)
		{
			onFixedUpdate -= act;
		}

		public void AddLateUpdateListener(Action act)
		{
			onLateUpdate += act;
		}

		public void RemoveLateUpdateListener(Action act)
		{
			onLateUpdate -= act;
		}
	}
}

