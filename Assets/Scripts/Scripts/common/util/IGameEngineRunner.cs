﻿using System;
using UnityEngine;
using System.Collections;

namespace Util
{
	public interface IGameEngineRunner
	{
		Coroutine StartCoroutine(IEnumerator method);
        void StopCoroutine(IEnumerator method);
        void AddFixedUpdateListener(Action act);
        void AddLateUpdateListener(Action act);
        void AddUpdateListener(Action act);
        void RemoveFixedUpdateListener(Action act);
        void RemoveLateUpdateListener(Action act);
        void RemoveUpdateListener(Action act);
    }
}

