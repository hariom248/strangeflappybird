﻿using strange.extensions.mediation.impl;
using UnityEngine;
using Util;

public class CameraView : View
{

}

public class CameraViewMediator : Mediator
{
    [Inject]
    public CameraView cameraView { get; set; }

    [Inject]
    public IGameEngineRunner gameEngineRunner { get; set; }

    [Inject]
    public BirdModel birdModel { get; set; }

    public override void OnRegister()
    {
        base.OnRegister();
        gameEngineRunner.AddLateUpdateListener(OnLateUpdate);
    }
    private void OnLateUpdate()
    {
        cameraView.transform.position = new Vector3(birdModel.Position.x,
                                         transform.position.y,
                                         transform.position.z);
    }

    public override void OnRemove()
    {
        base.OnRemove();
        gameEngineRunner.RemoveLateUpdateListener(OnLateUpdate);
    }
}