using strange.extensions.mediation.impl;
using strange.extensions.signal.impl;
using UnityEngine;
using Util;

public class BirdView : View
{
    public Rigidbody2D birdObj;
    public Signal collisionSignal = new Signal();
    public void OnCollisionEnter2D() => collisionSignal.Dispatch();
}

public class BirdViewMediator : Mediator
{
    [Inject]
    public BirdView birdView { get; set; }

    [Inject]
    public GameInputSignal userInput { get; set; }

    [Inject]
    public GameRestartSignal gameRestartSignal { get; set; }

    [Inject]
    public BirdModel birdModel { get; set; }

    [Inject]
    public IGameEngineRunner gameEngineRunner { get; set; }

    private const float speed = 2;

    // Flap force
    private const float force = 300;

    public override void OnRegister()
    {
        base.OnRegister();

        userInput.AddListener(InputReceived);
        gameEngineRunner.AddFixedUpdateListener(FixedUpdateListener);
        birdView.collisionSignal.AddListener(CollisionDetected);
        birdView.birdObj.velocity = Vector2.right * speed;
    }

    private void FixedUpdateListener()
    {
        birdModel.Position = birdView.transform.position;
    }

    private void CollisionDetected()
    {
        gameRestartSignal.Dispatch();
    }

    private void InputReceived()
    {
        birdView.birdObj.AddForce(Vector2.up * force);
    }

    public override void OnRemove()
    {
        base.OnRemove();
        gameEngineRunner.RemoveFixedUpdateListener(FixedUpdateListener);
        birdView.collisionSignal.RemoveListener(CollisionDetected);
        userInput.RemoveListener(InputReceived);
    }
}
