﻿using strange.extensions.mediation.impl;
using System.Collections;
using UnityEngine;
using Util;

public class ObstacleView : View
{
    public Rigidbody2D rigidbody;
}
public class ObstacleViewMediator : Mediator
{
    [Inject]
    public ObstacleView obstacleView { get; set; }

    [Inject]
    public IGameEngineRunner gameEngineRunner { get; set; }

    public override void OnRegister()
    {
        base.OnRegister();
        obstacleView.rigidbody.velocity = Vector2.up;
        gameEngineRunner.StartCoroutine(Move());
    }

    public override void OnRemove()
    {
        base.OnRemove();
        gameEngineRunner.StopCoroutine(Move());
    }

    private IEnumerator Move()
    {
        while(true)
        {
            yield return new WaitForSeconds(2f);
            obstacleView.rigidbody.velocity *= -1;
        }
    }
}
